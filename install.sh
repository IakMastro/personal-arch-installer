#!/bin/bash

echo "===> Installing dependencies"

pacman -Syu --noconfirm sudo xfce4 xfce4-goodies git base-devel plank lightdm lightdm-webkit2-greeter net-tools dhcpcd NetworkManager chrony vim zsh

echo "===> Enabling services"

systemctl enable dhcpcd
systemctl enable NetworkManager
systemctl enable chrony
systemctl enable lightdm

echo "===> Creating user"

echo "Give name for the user: "

read username

useradd -m $username

echo "Give a password"

passwd $username

echo "$username			ALL=(ALL) ALL" >> /etc/sudoers

echo "===> Downloading yay"

cd /tmp

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

echo "===> Installing XFCE4 themes"

yay -S --noconfirm qogir-gtk-theme qogir-icon-theme ulauncher yay -Syu lightdm-webkit2-theme-glorious

echo "====> Configurating lightdm"

sudo sed -i 's/^\(#?greeter\)-session\s*=\s*\(.*\)/greeter-session = lightdm-webkit2-greeter #\1/ #\2g' /etc/lightdm/lightdm.conf

# Set default lightdm-webkit2-greeter theme to Glorious
sudo sed -i 's/^webkit_theme\s*=\s*\(.*\)/webkit_theme = glorious #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf
sudo sed -i 's/^debug_mode\s*=\s*\(.*\)/debug_mode = true #\1/g' /etc/lightdm/lightdm-webkit2-greeter.conf

echo "===> Switching to user"

su $username

echo "===> Installing oh-my-zsh"

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "===> Installing vim plugins"

cd /tmp

git clone https://gitlab.com/dianshane/vim

cd vim
./install_new.sh

echo "Run vim to finish the install!"

echo "===> Done!"
